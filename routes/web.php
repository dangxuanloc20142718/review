<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get("abcd/{name}",function($name){


Route::get('test','TestController@test')->name('test');
Route::get('abcd',function(){
    return view('Abcd');
});
Route::get('banner/index',function(){
    return view('banner.index');
});
Route::get('addCart','ProductController@addCart')->name('addCart');
Route::resource('product/index','ProductController');
Route::get('dialog',function(){
    return view(
        'Dialog');
});
Route::get('ggmap',function(){
    return view('ProcessFile');
});
Route::resource('testGgmap','GoogleMapController');
Route::get('test',function(){
    return view('test');
});
Route::get('testValidate','ValidateTestController@index');
Route::get('validate','ValidateTestController@test')->name('validate');
Route::get('test1',function(){
    return view('test1');
});
Route::get('newTest',function(){
    return view('new');
});
Route::get('test1234','ItemController@index');
Route::get('viewListItem','ItemController@view')->name('view');
Route::get('example',function(){
    $dom =  file_get_html('https://tiki.vn/dien-thoai-smartphone/c1795?src=static_block&_lc=Vk4wMzQwMjIwMTI%3D');
    echo $dom;

});
