
<!DOCTYPE html>
<html lang="ja">
<head>
   @include('element.meta')
</head>
<body>
<a id="pagetop"></a>

<div id="header">
  @include('element.header')
    <!--headerIn -->
</div>
<!--header -->

<div id="Contentwrap">
   @yield('content')
    <!--PageContent -->

</div>
<!--Contentwrap -->

@include('element.footer')


</body>
</html>
