<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.css">
<style>
    html, body, div, span, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, abbr, address, cite, code, del, dfn, em, img, ins, kbd, q, samp, small, strong, sub, sup, var, b, i, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, dialog, figure, footer, header, hgroup, menu, nav, section, time, mark, audio, video {
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    font-size: 100%;
    vertical-align: baseline;
    background: transparent;
    color: black;
    }
    input {
        border: solid 1px #c9c9c9;
    }
    body {
        background: url('image/bg.gif')
    }
   .header {
    background-color: #fce30c;
    }
   .headerIn {
    width: 1100px;
    position: relative;
    margin: auto;
    height: 105px
   }
   #logo {
    position: absolute;
    left: 0;
    top: 8px;
   }
   #headerNav {
    position: absolute;
    left: 150px;
    top:12px;
   }
   #headerNav .nav01 > li {
    list-style-type: none;
    float: left;
    width: 120px;
    text-align: center;
    height: 88px;
    font-size: 14px;
    position: relative;
   }
   #headerNav .nav01 li img {
     margin-top: 5px;
   }

   #headerNav .nav01 li span {
       position: absolute;
       top:70%;
       left: 50%;
       transform: translate(-50%, 0px);
   }
   .nav02 {
        position: absolute;
        top: 100px;
        left: 0px;
        display: none;
   }
   .nav02 li {
    list-style-type: none;
    line-height: 45px;
    height: 45px;
    width: 120px;
    margin-bottom: 1px;
   }
   .nav02 li a {
    text-decoration: none;
    background: black;
    color: #fff;
    margin-bottom: 10px;
    display: block;
    margin-left: 2px;
   }
   .nav01 > li:hover .nav02 {
        display: block;
   }
   .Contentwrap {
    width: 1100px;
    margin: 0 auto;
    overflow: hidden;
    padding: 15px 0;
   }
   #PageContent {
    background-color: #fff;
    padding: 24px;
    margin-bottom: 49px;
   }
   #PageContent h2 {
    font-size: 14px;
    color: black;
    padding: 0px;
    border-bottom: 1px solid #DCC29B;
    margin: 10px 0 20px;
    font-weight: normal;
   }
   .fa-search::before {
    content: "\f002";
   }
   table {
    width: 100%;
   }
   table tr td {
    padding: 15px 10px 38px 0;
    text-align: left;
    white-space: nowrap;
    vertical-align: middle
   }
   input[type='radio'] {
    display: none;
   }
   .order label {
    display: block;
    float: left;
    cursor: pointer;
    min-width: 90px;
    margin: 1px;
    background-color: #eee;
    padding: 14px 4px;
    color: #bbb;
    font-size: 15px;
    font-weight: bold;
    text-align: center;
    line-height: 1;
    transition: .2s;
    box-sizing: border-box;
   }
   .order input {
    height: 48px;
    line-height: 48px;
    padding: 0 10px;
    box-sizing: border-box;
    border-radius: 5px
   }
    .inline {
        display: inline-block;
        min-width: 122px;
        height: 48px;
    }
    .inline select {
    width: 100%;
    padding: 0.9em;
    padding-right: 30px;
    box-sizing: border-box;
    z-index: 10;
    border-radius: 4px;
    }
    .active {
        background-color: #212529 !important;
        color:white !important;
    }
    .PageContentNav {
        overflow: hidden;
        width: 100%;
        padding: 15px 0 20px;
    }
    .OrderForm th,.OrderForm td,.OrderForm {
        border: solid 1px #c9c9c9;
        box-sizing: border-box;
        padding: 7px 2px;
        border-collapse: collapse;
        text-align: center;
    }
    .OrderForm tr:first-child{
        background-color: #faf8f4 !important;
    }
    .OrderForm tr:nth-child(odd){
        background-color: #eee;
    }
    @media screen and (max-width: 768px){
       .reponsive-td td{
        padding: 5px 10px 5px 0;
        display: block;
       }
       .reponsive-td td input {
         width: calc(100% - 150px);
       }
       .date-start {
        display: block;
       }
       .align-left {
        float: left;
       }

    }


  
  


</style>
</head>
<body>

<div class="header">
    <div class="headerIn">
        <p id="logo"> 
            <img src="{{asset('/image/logo-pc.png')}}">
        </p>
        <div id="headerNav">
            <ul class="nav01 menu__multi">
                <li><a href="#"><img src="{{asset('/image/order.png')}}" /><span>注文管理</span></a>
                    <ul class="nav02 menu__multi">
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                    </ul>
                </li>
                <li><a href="#"><img src="{{asset('/image/category.png')}}" /><span>注文管理</span></a>
                    <ul class="nav02 menu__multi">
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                    </ul>
                </li>
                <li><a href="#"><img src="{{asset('/image/option.png')}}" /><span>注文管理</span></a>
                    <ul class="nav02 menu__multi">
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                    </ul>
                </li>
                <li><a href="#"><img src="{{asset('/image/setting.png')}}" /><span>注文管理</span></a>
                    <ul class="nav02 menu__multi">
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                    </ul></li>
                <li><a href="#"><img src="{{asset('/image/notification.png')}}" /><span>注文管理</span></a>
                    <ul class="nav02 menu__multi">
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                    </ul>
                </li>
                <li><a href="#"><img src="{{asset('/image/product.png')}}" /><span>注文管理</span></a>
                    <ul class="nav02 menu__multi">
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                    </ul>
                </li>
                <li><a href="#"><img src="{{asset('/image/user.png')}}" /><span>注文管理</span></a>
                    <ul class="nav02 menu__multi">
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                        <li><a href="#">注文一覧</a></li>
                    </ul>
                </li>
            </ul>
        </div>
       
    </div>
</div>

<div class="Contentwrap">
    <div id='PageContent'>
        <h2>
            <i class="fa-search">注文検索</i>
        </h2>
        <form>
            <div class="order">
                <table>
                    <tbody>
                        <tr class="reponsive-td">
                            <td>ブランド</td>
                            <td class="branch" colspan="3">
                                <input type="radio" name="">
                                <label>全て</label>
                                <input type="radio" name="">
                                <label>上間フードアンドライフ</label>
                                 <input type="radio" name="">
                                <label>吉野家</label>
                                 <input type="radio" name="">
                                <label>松屋</label>
                            </td>
                        </tr>
                        <tr class="reponsive-td">
                            <td>ユーザーID</td>
                            <td><input type="text" placeholder="ユーザーID" name=""></td>
                            <td>ユーザー名</td>
                            <td><input type="text" placeholder="ユーザー名" name=""></td>
                        </tr>
                        <tr class="reponsive-td">
                            <td>注文ID</td>
                            <td><input type="text" placeholder="注文ID" name=""></td>
                            <td>電話番号</td>
                            <td><input type="text" placeholder="電話番号" name=""></td>
                        </tr>
                         <tr>
                            <td class="date-start">注文日時</td>
                            <td colspan="3" class="align-left">
                                <div class="inline date-start">
                                    <input class="date-picker" type="text"  placeholder="例：2019/01/02" name="">
                                </div> 
                                <div class="inline">
                                    <select name="timeOrderBegin">
                                        <option>00時</option>
                                        <option>01時</option>
                                        <option>02時</option>
                                        <option>03時</option>
                                        <option>04時</option>
                                        <option>05時</option>
                                        <option>06時</option>
                                        <option>07時</option>
                                    </select>
                                </div>
                                &nbsp;〜&nbsp;
                                <div class="inline date-start">
                                    <input class="date-picker" type="text"  placeholder="例：2019/01/02" name="">
                                </div> 
                                <div class="inline">
                                    <select name="timeOrderBegin">
                                        <option>00時</option>
                                        <option>01時</option>
                                        <option>02時</option>
                                        <option>03時</option>
                                        <option>04時</option>
                                        <option>05時</option>
                                        <option>06時</option>
                                        <option>07時</option>
                                    </select>
                                </div>

                            </td>
                        </tr>
                        <tr>
                            <td class="date-start">引渡日時</td>
                            <td colspan="3" class="align-left">
                                <div class="inline date-start">
                                    <input class="date-picker" type="text"  placeholder="例：2019/01/02" name="">
                                </div> 
                                <div class="inline">
                                    <select name="timeOrderBegin">
                                        <option>00時</option>
                                        <option>01時</option>
                                        <option>02時</option>
                                        <option>03時</option>
                                        <option>04時</option>
                                        <option>05時</option>
                                        <option>06時</option>
                                        <option>07時</option>
                                    </select>
                                </div>
                                &nbsp;〜&nbsp;
                                <div class="inline date-start">
                                    <input class="date-picker" type="text"  placeholder="例：2019/01/02" name="">
                                </div> 
                                <div class="inline">
                                    <select name="timeOrderBegin">
                                        <option>00時</option>
                                        <option>01時</option>
                                        <option>02時</option>
                                        <option>03時</option>
                                        <option>04時</option>
                                        <option>05時</option>
                                        <option>06時</option>
                                        <option>07時</option>
                                    </select>
                                </div>

                            </td>
                        </tr>
                       <tr>
                            <td>ブランド</td>
                            <td colspan="3" class="category">
                                <input type="radio" name="">
                                <label>全て</label>
                                <input type="radio" name="">
                                <label>受注</label>
                                 <input type="radio" name="">
                                <label>製造済</label>
                                 <input type="radio" name="">
                                <label>引渡済</label>
                                 <input type="radio" name="">
                                <label>キャンセル</label>
                                 <input type="radio" name="">
                                <label>請求確定</label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="contentfoot"></div>
        </form>
    </div>
    <div id='PageContent'>
        <h2>
            <i class="fa-search">注文一覧</i>
        </h2>
        <div class="PageContentNav">
            0件中<strong>0～0</strong>件目を表示しています
        </div>
        <table class="OrderForm">
            <tr class="head">
                <td>注文ID</td>
                <td>ステータス</td>
                <td>注文日時 <br/> 引渡日時</td>
                <td>ブランド <br/> 店舗</td>
                <td>ユーザーID <br/>ユーザー名</td>
                <td>電話番号</td>
                <td>金額</td>
                <td>詳細</td>
            </tr>
            <tr>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
            </tr>
            <tr>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
            </tr>
            <tr>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
            </tr>
            <tr>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
            </tr>
            <tr>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
                <td>test1</td>
            </tr>
        </table>
    </div>
</div>
 

</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
</script>
<script type="text/javascript" src="jquery-ui/jquery-ui.js"></script>
<script type="text/javascript">
    $('.branch label').click(function(){
         var list = $('.branch label');
         list.each(function(){
                $(this).removeClass('active');
         });
        $(this).addClass('active');
    });
     $('.category label').click(function(){
         var list = $('.category label');
         list.each(function(){
                $(this).removeClass('active');
         });
        $(this).addClass('active');
    });

     $(function(){
        $( ".date-picker" ).datepicker({
           dateFormat: "<yy></yy>-mm-dd"
        });
     });
    

</script>