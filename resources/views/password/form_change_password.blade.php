@extends('frontend.layout.index')
@section('content')
    <div class="container">
        <div id="content">
            <form action="{{route('displayForm')}}" method="post" class="beta-form-checkout">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <h4>Reset Password</h4>
                        <div class="space20">&nbsp;</div>
                        @if(Session::has('messages'))
                            <div class="form-block">
                                <div class="alert alert-primary" role="alert" style="width: 540px" >
                                    {{Session::get('messages')}}
                                </div>
                            </div>
                        @endif
                        <input type="hidden" name="token" value="{{$token}}">
                        <div class="form-block">
                            <label for="password">New Password*</label>
                            <input type="password" name="password" id="password" >
                        </div>
                        @if(!empty($errors->first('password')))
                            <div class="form-block">
                                <div class="alert alert-primary" role="alert" style="width: 540px" >
                                    {{$errors->first('password')}}
                                </div>
                            </div>
                        @endif

                        <div class="form-block">
                            <label for="confirm-password">Confirm Password*</label>
                            <input type="password" name="confirm_password" id="confirm_password" >
                        </div>

                        @if(!empty($errors->first('confirm_password')))
                            <div class="form-block">
                                <div class="alert alert-primary" role="alert" style="width: 540px" >
                                    {{$errors->first('confirm_password')}}
                                </div>
                            </div>
                        @endif

                        <div class="form-block form-forgot">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </form>
        </div> <!-- #content -->
    </div> <!-- .container --
@endsection
