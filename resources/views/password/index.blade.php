@extends('frontend.layout.index')
@section('content')
    <div class="container">
        <div id="content">
            <form action="{{route('postForgot')}}" method="post" class="beta-form-checkout">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <h4>Quên mật khẩu</h4>
                        <div class="space20">&nbsp;</div>
                        @if(Session::has('messages'))
                            <div class="form-block">
                                <div class="alert alert-primary" role="alert" style="width: 540px" >
                                    {{Session::get('messages')}}
                                </div>
                            </div>
                        @endif
                        <div class="form-block">
                            <label for="email">Email address*</label>
                            <input type="text" name="email" id="email" >
                        </div>
                        <div class="form-block form-forgot">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </form>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection
