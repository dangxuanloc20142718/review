<!DOCTYPE html>
<html lang=" ">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style type="">
		  /**{
				margin:0px;
				padding: 0px;
			}
		  #container{
			width: 600px;
			margin: 20px auto;
		  }
           ul {
           	list-style-type: none;
           	overflow:auto;
           	background-color: #333333;
           }
           ul li{
           	float: left;
           	width: 60px;
           	height: 40px;
           	line-height: 40px;
           }
           ul li a{
           	display: block;
            color:white;
            padding-left: 10px;
           }
           ul li a:hover {
           	color: white;
           }*/
           * {
				margin:0px;
				padding: 0px;
				box-sizing: border-box;
			}
          .content1 {
          	width: 80%;
          	margin: 20px auto;
          }
          .header ,.footer {
          	height: 107px;
          	padding: 15px;
          	background-color: #808080;
          	color: white;
          }
          .content{
			/*overflow: auto;*/
			margin-top: 10px;
			margin-left: 15px;
			display: flex;
			border: 1px solid red;
          }
          .left {
          	/*float: left;*/
          	width: 25%;
          	border: 1px solid black;
          }
          .right {
          	/*float: left;*/
          	width: 75%;
          	padding-left: 15px;
          	border: 1px solid black;
          }
          ul {
			list-style-type: none;
          }
          ul li {
          	height: 33px;
          	line-height: 33px;
          	background-color: #33B5E5;
          	margin-top: 5px;
          	padding-left: 10px;
          }

			
		</style>
	</head>
	<body>
		<div id="container">
		    <ul>
		    	<li><a href="#" style="background-color: red">Home</a></li>
		    	<li><a href="#">New</a></li>
		    	<li><a href="#">Contact</a></li>
		    	<li><a href="#">About</a></li>
		    </ul>
	    </div>
	    <div class="content1">
	    	<div class="header">
	    		<h1>Chania</h1>
	    	</div>
	    	<div class="content">
	    		<div class="left">
	    			<ul>
	    				<li>The Flight</li>
	    				<li>The City</li>
	    				<li>The Island</li>
	    				<li>The Food</li>
	    			</ul>
	    		</div>
	    		<div class="right">
	    			<h1>The City</h1>
	    			<p>Chania is the capital of the Chania region on the island of Crete. The city can be divided in two parts, the old town and the modern city.</p>
	    			<p>You will learn more about web layout and responsive web pages in a later chapter</p>
	    		</div>
	    	</div>
	    	<div class="footer">
	    		<h4>Footer Text</h4>
	    	</div>
	    </div>


		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	</body>
</html>
