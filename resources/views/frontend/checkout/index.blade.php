@extends('frontend.layout.index')
@section('content')
    <div class="container">
        <div id="content">

            <form action="{{route('page.postCheckout')}}" method="post" class="beta-form-checkout">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Đặt hàng</h4>
                        <div class="space20">&nbsp;</div>

                        <div class="form-block">
                            <label for="name">Họ tên*</label>
                            <input type="text" id="name" name="name" placeholder="Họ tên" >
                        </div>
                        <div class="validate" >
                               @include('element.validate_error',['message' => $errors->first('name')])
                        </div>
                        <div class="form-block">
                            <label>Giới tính </label>
                            <input id="gender" type="radio"  class="input-radio" name="gender" value="1" checked="checked" style="width: 10%"><span style="margin-right: 10%">Nam</span>
                            <input id="gender" type="radio" class="input-radio" name="gender" value="0" style="width: 10%"><span>Nữ</span>

                        </div>

                        <div class="form-block">
                            <label for="email">Email*</label>
                            <input type="email" id="email" name="email" required placeholder="expample@gmail.com">
                        </div>
                        <div class="validate" >
                            @include('element.validate_error',['message' => $errors->first('email')])
                        </div>

                        <div class="form-block">
                            <label for="adress">Địa chỉ*</label>
                            <input type="text" id="adress" name="address" placeholder="Street Address">
                        </div>
                        <div class="validate" >
                            @include('element.validate_error',['message' => $errors->first('address')])
                        </div>

                        <div class="form-block">
                            <label for="phone">Điện thoại*</label>
                            <input type="text" id="phone" name="phone_number" >
                        </div>
                        <div class="validate" >
                            @include('element.validate_error',['message' => $errors->first('phone')])
                        </div>

                        <div class="form-block">
                            <label for="notes">Ghi chú</label>
                            <textarea id="notes" name="note"></textarea>
                        </div>
                        @include('element.message')
                    </div>
                    <div class="col-sm-6">
                        <div class="your-order">
                            <div class="your-order-head"><h5>Đơn hàng của bạn</h5></div>
                            <div class="your-order-body" style="padding: 0px 10px">
                                @foreach(Session::get('cart')->items as $key => $item )
                                <div class="your-order-item checkout-{{$key}}">
                                    <div>
                                        <!--  one item	 -->
                                        <div class="media">
                                            <img width="25%" src="image/product/{{$item['item']->image}}" alt="" class="pull-left">
                                            <div class="media-body">
                                                <p class="font-large">{{$item['item']->name}}</p>
                                                <span class="color-gray your-order-info">Color: Red</span>
                                                <span class="color-gray your-order-info">Size: M</span>
                                                <span class="color-gray your-order-info">Qty: {{$item['qty']}}</span>
                                            </div>
                                        </div>
                                        <!-- end one item -->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                @endforeach
                                <div class="your-order-item">
                                    <div class="pull-left"><p class="your-order-f18">Tổng tiền:</p></div>
                                    <div class="pull-right"><h5 class="color-black checkout-total">${{Session::get('cart')->totalPrice + 30}}</h5></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="your-order-head"><h5>Hình thức thanh toán</h5></div>

                            <div class="your-order-body">
                                <ul class="payment_methods methods">
                                    <li class="payment_method_bacs">
                                        <input id="payment_method_bacs" type="radio"  class="input-radio" name="payment" value="COD" checked="checked" data-order_button_text="">
                                        <label for="payment_method_bacs">Thanh toán khi nhận hàng </label>
                                        <div class="payment_box payment_method_bacs" style="display: block;">
                                            Cửa hàng sẽ gửi hàng đến địa chỉ của bạn, bạn xem hàng rồi thanh toán tiền cho nhân viên giao hàng
                                        </div>
                                    </li>

                                    <li class="payment_method_cheque">
                                        <input id="payment_method_cheque" type="radio" class="input-radio" name="payment" value="ATM" data-order_button_text="">
                                        <label for="payment_method_cheque">Chuyển khoản </label>
                                        <div class="payment_box payment_method_cheque" style="display: none;">
                                            Chuyển tiền đến tài khoản sau:
                                            <br>- Số tài khoản: 123 456 789
                                            <br>- Chủ TK: Nguyễn A
                                            <br>- Ngân hàng ACB, Chi nhánh TPHCM
                                        </div>
                                    </li>

                                </ul>
                            </div>

                            <div class="text-center"><button class="beta-btn primary" type="submit">Đặt hàng <i class="fa fa-chevron-right"></i></button></div>
                        </div> <!-- .your-order -->
                    </div>
                </div>
            </form>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection
