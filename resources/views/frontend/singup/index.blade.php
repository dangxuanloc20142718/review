@extends('frontend.layout.index')
@section('content')
    <div class="container">
        <div id="content">
            <form action="{{route('page.postSingUp')}}" method="post" class="beta-form-checkout">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <h4>Đăng kí</h4>
                        <div class="space20">&nbsp;</div>
                        @if(Session::has('messages'))
                        <div class="form-block">
                            <div class="alert alert-primary" role="alert" style="width: 540px" >
                                {{Session::get('messages')}}
                            </div>
                        </div>
                        @endif
                        <div class="form-block">
                            <label for="email">Email address*</label>
                            <input type="email" name='email' id="email" value="{{old('email')}}" >
                        </div>
                        @include('element.validation',['errors' =>$errors->first('email')])
                        <div class="form-block">
                            <label for="your_last_name">Fullname*</label>
                            <input type="text" name="full_name" id="your_last_name" value="{{old('full_name')}}" >
                        </div>
                        @include('element.validation',['errors' =>$errors->first('full_name')])
                        <div class="form-block">
                            <label for="address">Address*</label>
                            <input type="text" name="address" id="adress" value="{{old('address')}}" >
                        </div>
                        @include('element.validation',['errors' =>$errors->first('address')])

                        <div class="form-block">
                            <label for="phone">Phone*</label>
                            <input type="text" name="phone" id="phone" value="{{old('phone')}}">
                        </div>
                        @include('element.validation',['errors' =>$errors->first('phone')])
                        <div class="form-block">
                            <label for="password">Password*</label>
                            <input type="password" name="password" id="password" value="{{old('password')}}" class="height-37px">
                        </div>
                        @include('element.validation',['errors' =>$errors->first('password')])
                        <div class="form-block ">
                            <label for="repassword">Re password*</label>
                            <input type="password" name="repassword" id="repassword" value="{{old('repassword')}}" class="height-37px">
                        </div>
                        @include('element.validation',['errors' =>$errors->first('repassword')])
                        <div class="form-block ">
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </form>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection
