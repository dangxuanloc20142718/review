@extends('frontend.layout.index')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Product</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb font-large">
                    <a href="index.html">Home</a> / <span>Product</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="container">
        <div id="content">
            <div class="row">
                <div class="col-sm-9">

                    <div class="row">
                        <div class="col-sm-4">
                            <img src="image/product/{{$product->image}}" alt="">
                        </div>
                        <div class="col-sm-8">
                            <div class="single-item-body">
                                <p class="single-item-title">{{$product->name}}</p>
                                <p class="single-item-price">
                                    @php
                                        if($product->promotion_price == 0){
                                           echo '$'.$product->unit_price;
                                        }else{
                                            echo '<span>$'.$product->promotion_price.'</span>'.' <span
                                        class="line-through">$'.$product->unit_price.'</span>';
                                        }
                                    @endphp

                                </p>
                            </div>

                            <div class="clearfix"></div>
                            <div class="space20">&nbsp;</div>

                            <div class="single-item-desc">
                                <p>{{$product->description}}</p>
                            </div>
                            <div class="space20">&nbsp;</div>

                            <p>Options:</p>
                            <div class="single-item-options">
                                <select class="wc-select" name="size">
                                    <option>Size</option>
                                    <option value="XS">XS</option>
                                    <option value="S">S</option>
                                    <option value="M">M</option>
                                    <option value="L">L</option>
                                    <option value="XL">XL</option>
                                </select>
                                <select class="wc-select" name="color">
                                    <option>Color</option>
                                    <option value="Red">Red</option>
                                    <option value="Green">Green</option>
                                    <option value="Yellow">Yellow</option>
                                    <option value="Black">Black</option>
                                    <option value="White">White</option>
                                </select>
                                <select class="wc-select" name="color">
                                    <option>Qty</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                                <a class="add-to-cart" href="#"><i class="fa fa-shopping-cart"></i></a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="space40">&nbsp;</div>
                    <div class="woocommerce-tabs">
                        <ul class="tabs">
                            <li><a href="#tab-description">Description</a></li>
                            <li><a href="#tab-reviews">Reviews ({{count($listComment)}})</a></li>
                        </ul>

                        <div class="panel" id="tab-description">
                            <p>{{$product->description}}</p>
                        </div>
                        <div class="panel" id="tab-reviews">
                            @foreach($listComment as $comment)
                                <div class="comment-user">
                                    <p class="head"><i class="fa fa-user fa-lg"></i> {{$comment->full_name}}</p>
                                    <p>
                                        {{$comment->comment}}
                                    </p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="space50">&nbsp;</div>
                    <div class="beta-products-list">
                        <h4>Related Products</h4>
                        <div class="row">
                            @foreach($listProductRelative as $product)
                                <div class="col-sm-4">
                                    <div class="single-item">
                                        <div class="single-item-header">
                                            <a href="{{route('page.detail',$product->id)}}"><img
                                                    src="{{asset('image/product/'.$product->image)}}"
                                                    alt=""></a>
                                        </div>
                                        <div class="single-item-body">
                                            <p class="single-item-title">{{$product->name}}</p>
                                            <p class="single-item-price">
                                                <span>${{$product->unit_price}}</span>
                                            </p>
                                        </div>
                                        <div class="single-item-caption">
                                            <a class="add-to-cart pull-left" href="product.html"><i
                                                    class="fa fa-shopping-cart"></i></a>
                                            <a class="beta-btn primary" href="{{route('page.detail',$product->id)}}">Details
                                                <i
                                                    class="fa fa-chevron-right"></i></a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div> <!-- .beta-products-list -->
                    <div>{{$listProductRelative->links()}}</div>
                </div>
                <div class="col-sm-3 aside">
                    <div class="widget">
                        <h3 class="widget-title">Best Sellers</h3>
                        <div class="widget-body">
                            <div class="beta-sales beta-lists">
                                @foreach($listProductBestSale as $product)
                                    <div class="media beta-sales-item">
                                        <a class="pull-left" href="{{route('page.detail',$product[1]->id)}}"><img
                                                src="{{asset('image/product/'.$product[1]->image)}}" alt=""></a>
                                        <div class="media-body">
                                       <span style="display: block"
                                             class="line-through">${{$product[1]->unit_price}}</span>
                                            <span class="beta-sales-price">${{$product[1]->promotion_price}}</span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div> <!-- best sellers widget -->
                    <div class="widget">
                        <h3 class="widget-title">New Products</h3>
                        <div class="widget-body">
                            <div class="beta-sales beta-lists">
                                @foreach($listNewProduct as $product)
                                    <div class="media beta-sales-item">
                                        <a class="pull-left" href="product.html"><img
                                                src="{{asset('image/product/'.$product->image)}}" alt=""></a>
                                        <div class="media-body">
                                            @if($product->promotion_price != 0)
                                                <span style="display: block"
                                                      class="line-through">${{$product->unit_price}}</span>
                                                <span class="beta-sales-price">${{$product->promotion_price}}</span>
                                                @else
                                                <span style="display: block"
                                                     >${{$product->unit_price}}</span>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div> <!-- best sellers widget -->
                </div>
            </div>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection
