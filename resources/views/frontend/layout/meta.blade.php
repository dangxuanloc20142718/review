<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
<title>Laravel </title>
<base href="{{asset('')}}">
<link href='http://fonts.googleapis.com/css?family=Dosis:300,400' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/dest/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/dest/vendors/colorbox/example3/colorbox.css">
<link rel="stylesheet" href="assets/dest/rs-plugin/css/settings.css">
<link rel="stylesheet" href="assets/dest/rs-plugin/css/responsive.css">
<link rel="stylesheet" title="style" href="assets/dest/css/style.css">
<link rel="stylesheet" href="assets/dest/css/animate.css">
<link rel="stylesheet" title="style" href="assets/dest/css/huong-style.css">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="css/customize.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<style>
    .single-item-header {
        height: 265px;
    }
    .single-item-header img {
        height: 265px;
    }
    .single-item-price {
        margin: 4px auto;
    }
    .line-through {
        text-decoration: line-through;
    }
    .single-item {
        margin: 4px auto;
    }
    .comment-user {
        width: 100%;
        border-bottom: 1px solid #f6f6f6;
    }
    .comment-user .head {
        font-weight: bold;
    }
    .validation {
        width: 324px;
        float: right;
        margin-right: 15px;
        overflow: hidden;
    }
    .height-37px {
        height: 37px;
        border: 1px solid #e1e1e1;
    }
    .cart-item {
        position: relative;
    }
    .close {
        position: absolute;
        top: 0;
        right: 0;
        padding: 10px;
    }
    .close:hover{
        background: #6d6d6d;
    }
    .product-quantity {
        display: flex;
    }
    .change-quantity {
        width: 40px !important;
    }
    .change-qty {
        width: 30px;
        display: inline-block;
        border: 1px solid #e1e1e1;
        cursor: pointer;
    }
    .primary:hover {
        text-decoration: none;
    }
    .hidden {
        display: none;
    }
    .red-text {
        color: red;
        margin-right: 16px;
        float: right;
        width: 60%;
    }
    .validate {
        overflow: auto;
    }


</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

