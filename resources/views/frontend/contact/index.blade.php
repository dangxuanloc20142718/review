@extends('frontend.layout.index')
@section('content')

    <div id="map" style="height: 500px; width: 100%; margin-top: 10px;"></div>
    <div class="container">
        <div id="content" class="space-top-none">
            <div class="space50">&nbsp;</div>
            <div class="row">
                <div class="col-sm-8">
                    <h2>Contact Form</h2>
                    <div class="space20">&nbsp;</div>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                        deserunt mollit ani m id est laborum.</p>
                    <div class="space20">&nbsp;</div>
                    @if(Session::has('flash_message'))
                        <div class="alert alert-primary" role="alert">
                            {{Session::get('flash_message')}}
                        </div>
                    @endif
                    <form action="{{route('page.sendMail')}}" method="post" class="contact-form">
                        {{csrf_field()}}
                        <div class="form-block">
                            <input name="name" type="text" placeholder="Your Name (required)" value="{{old('name')}}">
                        </div>
                        <div class="form-block">
                            <input name="email" type="email" placeholder="Your Email (required)" value="{{old('email')}}">
                        </div>
                        <div class="form-block">
                            <input name="subject" type="text" placeholder="Subject" value="{{old('subject')}}">
                        </div>
                        <div class="form-block">
                            <textarea name="message" placeholder="Your Message">{{old('message')}}</textarea>
                        </div>
                        <div class="form-block">
                            <button type="submit" class="beta-btn primary">Send Message <i
                                    class="fa fa-chevron-right"></i></button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-4">
                    <h2>Contact Information</h2>
                    <div class="space20">&nbsp;</div>

                    <h6 class="contact-title">Address</h6>
                    <p>
                        Suite 127 / 267 – 277 Brussel St,<br>
                        62 Croydon, NYC <br>
                        Newyork
                    </p>
                    <div class="space20">&nbsp;</div>
                    <h6 class="contact-title">Business Enquiries</h6>
                    <p>
                        Doloremque laudantium, totam rem aperiam, <br>
                        inventore veritatio beatae. <br>
                        <a href="mailto:biz@betadesign.com">biz@betadesign.com</a>
                    </p>
                    <div class="space20">&nbsp;</div>
                    <h6 class="contact-title">Employment</h6>
                    <p>
                        We’re always looking for talented persons to <br>
                        join our team. <br>
                        <a href="hr@betadesign.com">hr@betadesign.com</a>
                    </p>
                </div>
            </div>
        </div> <!-- #content -->
    </div> <!-- .container -->

    <script>
        function initMap() {
            var myLatlng = new google.maps.LatLng(21.0227788, 105.8194541);
            var mapOptions = {
                zoom: 12,
                center: myLatlng,
                mapTypeId: 'roadmap',
            };
            var map = new google.maps.Map(document.getElementById('map'),
                mapOptions);
            var hoan_kiem_lake = {lat: 21.0287797, lng: 105.850176};
            var marker = new google.maps.Marker({position: hoan_kiem_lake, map: map});

        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc_z85PWzLEimF7jVXDGeJV6JmOB5JIaA&callback=initMap&language=vi">
    </script>
@endsection
