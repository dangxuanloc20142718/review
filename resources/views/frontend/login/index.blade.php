@extends('frontend.layout.index')
@section('content')
    <div class="container">
        <div id="content">

            <form action="{{route('postLogin')}}" method="post" class="beta-form-checkout">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <h4>Đăng nhập</h4>
                        <div class="space20">&nbsp;</div>
                        @if(Session::has('messages'))
                            <div class="form-block">
                                <div class="alert alert-primary" role="alert" style="width: 540px" >
                                    {{Session::get('messages')}}
                                </div>
                            </div>
                        @endif
                        <div class="form-block">
                            <label for="email">Email address*</label>
                            <input type="text" name="email" id="email" >
                        </div>
                        @if($errors->has('email'))
                        <div class="alert alert-danger" role="alert">
                           warning!  {{$errors->first('email')}}
                        </div>
                        @endif
                        <div class="form-block">
                            <label for="password">Password*</label>
                            <input type="password" name="password" id="password" >
                        </div>

                        @if($errors->has('password'))
                            <div class="alert alert-danger" role="alert">
                                warning!  {{$errors->first('password')}}
                            </div>
                        @endif

                        <div class="form-block form-forgot">
                            <button type="submit" class="btn btn-primary">Login</button><a href="{{route('auth.facebook')}}">FB Login</a><a class="forgot" href="{{route('getForgot')}}">forgot password</a>
                        </div>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </form>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection
<!--customjs-->

