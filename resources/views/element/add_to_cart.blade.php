<div class="beta-dropdown cart-body">
    @foreach($cart->items as $key => $product)
    <div class="cart-item">
        <div class="media">
            <a class="pull-left" href="#"><img src="image/product/{{$product['item']->image}}" alt=""></a>
            <div class="media-body">
                <span class="cart-item-title">{{$product['item']->name}}</span>
                <span class="cart-item-options">Size: XS; Colar: Navy</span>
                <span class="cart-item-amount">{{$product['item']['qty']}}*<span>${{$product['item']->unit_price}}</span></span>
                <span class="close" id="product-{{$key}}" onclick="deleteCart('{{$key}}','{{route('page.delete')}}')">&times;</span>
            </div>
        </div>
    </div>
    @endforeach
    <div class="cart-caption">
        <div class="cart-total text-right">Tổng tiền: <span class="cart-total-value">${{$cart->totalPrice}}</span></div>
        <div class="clearfix"></div>

        <div class="center">
            <div class="space10">&nbsp;</div>
            <a href="{{route('cart.detail')}}" class="beta-btn primary text-center">Đặt hàng <i class="fa fa-chevron-right"></i></a>
        </div>
    </div>
</div>
