@if(Session::has('success'))
    <div class="form-block" style="height: 30px; background-color: #6cb2eb;line-height: 30px;color: white;padding-left: 10px">
        {{Session::get('success')}}
    </div>
@endif
@if(Session::has('error'))
    <div class="form-block" style="height: 30px; background-color: #6cb2eb;line-height: 30px;color: red;padding-left: 10px">
        {{Session::get('error')}}
    </div>
@endif
