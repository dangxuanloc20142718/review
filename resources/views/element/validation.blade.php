@if($errors)
    <div class="form-block" >
        <div class="alert alert-danger validation" >
             {{$errors}}
        </div>
    </div>
@endif
