<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 7/1/2019
 * Time: 8:54 AM
 */

namespace App\Repositories;
use App\Models\Comment;

class CommentRepository extends BaseRepository
{
    public function __construct()
    {
        $this->_model = Comment::class;
    }
}
