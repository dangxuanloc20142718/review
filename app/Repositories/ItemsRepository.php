<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 9/12/2019
 * Time: 1:46 PM
 */

namespace App\Repositories;


use App\Models\Items;

class ItemsRepository extends BaseRepository
{
    public function __construct()
    {
        $this->_model = Items::class;
    }
    public function addItem(){
        $dom = file_get_html('https://vnexpress.net/bong-da');
        $listItems = $dom->find('.list_news');
        $items = [];
        foreach ($listItems as $key => $item){
            $title = $item->find('h4',0)->plaintext;
            $urlChild = $item->find('.thumb_art a',0)->href;
            $domChild = file_get_html($urlChild);
            $content = $domChild->find('.title_news_detail',0)->plaintext;
            $urlImage = $domChild->find('.tplCaption img',0);
            if(!empty($urlImage)){
                $image = $urlImage->src;
            }else{
                $image = null;
            }
            $item = ['title' => $title,'image' => $image,'content' => $content];
            $items [] = $item;
        }
        $this->addManyRecord($items);
    }
}
