<?php
/**
 * Created by PhpStorm.
 * User: MyPC
 * Date: 04/08/2019
 * Time: 4:39 CH
 */

namespace App\Repositories;


use App\Models\Bill_detail;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BillDetailRepository extends BaseRepository
{
    public function __construct()
    {
        $this->_model = Bill_detail::class;
    }
    public function addList($id_bill){
        $list = [];
        $cart = Session::get('cart')->items;
        foreach ($cart as $key => $value){
            $billDetail = [];
            $billDetail ['id_bill'] = $id_bill;
            $billDetail ['id_product'] = $key;
            $billDetail ['quantity'] = $value['qty'];
            $billDetail ['unit_price'] = $value['item']->unit_price;
            $billDetail ['ins_id'] = Auth::guard('frontend')->user()->id;
            $billDetail ['ins_date'] = Carbon::now();
            $list [] = $billDetail;
        }
        $this->_model::insert($list);
        return true;
    }

}
