<?php
/**
 * Created by PhpStorm.
 * User: MyPC
 * Date: 04/08/2019
 * Time: 8:29 CH
 */

namespace App\Repositories;


use App\Models\Customers;

class CustomerRepository extends BaseRepository
{
    public function __construct()
    {
        $this->_model = Customers::class;
    }
}
