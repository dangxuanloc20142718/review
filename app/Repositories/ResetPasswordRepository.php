<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 8/14/2019
 * Time: 11:37 AM
 */

namespace App\Repositories;


use App\Models\ResetPassword;

class ResetPasswordRepository extends BaseRepository
{
    public function __construct()
    {
        $this->_model = ResetPassword::class;
    }
    public function deleteArray($listReset){
        foreach ($listReset as $key => $reset){
            if($key == 0){
                continue;
            }
            $this->_model::where('email',$reset->email)->delete();
        }
    }

}
