<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 7/5/2019
 * Time: 1:32 PM
 */

namespace App\Repositories;


use App\Models\User;

class UsersRepository extends BaseRepository
{
    protected $resetpassword;
        public function __construct(ResetPasswordRepository $resetPassword)
        {
            $this->_model = User::class;
            $this->resetpassword = $resetPassword;
        }
        public function checkEmail($email){
            $user = $this->getModel()->where('email',$email)->get();
            if(count($user) != 0){
                return true;
            }else{
                return false;
            }
        }
        public function  updatePassword($email,$newPassword){
            $user = $this->getModel()->where('email',$email)->first();
            $listReset = $this->resetpassword->getModel()->where('email',$email)->orderBy('created_at','desc')->get();
            $this->resetpassword->deleteArray($listReset);
            $newPassword = bcrypt($newPassword);
            $user->password  = $newPassword;
            $user->save();
        }
}
