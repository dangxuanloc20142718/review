<?php
/**
 * Created by PhpStorm.
 * User: MyPC
 * Date: 04/08/2019
 * Time: 4:37 CH
 */

namespace App\Repositories;


use App\Models\Bills;

class BillRepository extends BaseRepository
{
    public function __construct()
    {
        $this->_model = Bills::class;
    }

}
