<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 6/27/2019
 * Time: 4:16 PM
 */

namespace App\Repositories;
use App\Models\Products;
use Illuminate\Support\Facades\DB;

class ProductRepository extends BaseRepository
{
    public function __construct(CommentRepository $commentRepository)
    {
        $this->_model = Products::class;
    }
    public function getNewProduct(){
        $listNewProduct = $this->_model::where('new',1)->orderBy('ins_date','DESC')->paginate(4);
        return $listNewProduct;
    }
    public function getTopProduct(){
        $listTopProduct = $this->_model::Where('promotion_price','!=',0)->orderBy('ins_date','DESC')->paginate(4);
        return $listTopProduct;
    }
    public function getCommentProduct($id){
      $listComment =  DB::table('comment')
           ->join('users','comment.id_user','users.id')
           ->where('comment.id_product',$id)
           ->select('comment.comment as comment','users.full_name as full_name')
           ->get();
      return $listComment;
    }
    public function getProductRelative($id){
        $id_type = $this->find($id)->id_type;
        $listProduct = $this->getModel()->where('id_type',$id_type)->whereNotIn('id',[$id])->orderBy('ins_date')->paginate(3);
        return $listProduct;
    }
    public function bestSale(){
         $listProduct = $this->getModel()->where('promotion_price','<>',0)->get();
         $arrSort = [];
         foreach ($listProduct as $product){
             $deviation = $product->unit_price - $product->promotion_price;
             $arrSort[] = [$deviation,$product];
         }
         rsort($arrSort);

         return array_slice($arrSort,0,4);
    }

}
