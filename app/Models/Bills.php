<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bills extends Model
{
    protected $table = 'bills';
    public  $timestamps = false;
    public $fillable = ['id_user','name','gender','email','address','phone_number','note','payment','ins_id','ups_id','ins_date','ups_date','date_order','total'];
    public function billDetail (){
        return $this->hasMany('app/Models/bill_detail');
    }
}
