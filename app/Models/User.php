<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Notifications\Notifiable;

class User  extends Model implements Authenticatable
{
    use Notifiable;
    use AuthenticableTrait;

    protected $table = 'users';
    public $timestamps = false;
    protected $fillable = array('full_name', 'password', 'email','phone','role','address','ins_id','ins_date');
}
