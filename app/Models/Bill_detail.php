<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill_detail extends Model
{
    protected $table = 'bill_detail';
    public $timestamps = false;
    public $fillable = [
            'id_bill',
            'id_product',
            'quantity',
            'unit_price',
            'ins_id',
            'ups_id',
            'ins_date',
            'ups_date'
    ];
}
