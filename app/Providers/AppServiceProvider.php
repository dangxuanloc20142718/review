<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        app('view')->composer('frontend.layout.index', function ($view) {
            $action = app('request')->route();
            if (!$action){
                return;
            } else {
                $action = $action->getAction();
            }
            $controller = class_basename($action['controller']);
            $controller = strtolower(str_replace('Controller', '', $controller));

            list($controller, $action) = explode('@', $controller);

            $view->with(compact('controller', 'action'));
        });
    }
}
