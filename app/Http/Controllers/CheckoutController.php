<?php

namespace App\Http\Controllers;

use App\Repositories\BillDetailRepository;
use App\Repositories\BillRepository;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Models\Cart;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class CheckoutController extends Controller
{
    public $billRepository;
    public $billDetailRepository;
    public function __construct(BillRepository $billRepository,BillDetailRepository $billDetailRepository)
    {
        $this->billRepository = $billRepository;
        $this->billDetailRepository = $billDetailRepository;

    }

    public function getCheckout(){
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('frontend.checkout.index');
    }
    public function postCheckout(Request $request){
        $bill = $request->only('name','gender','email','address','phone_number','note','payment');
        $bill['date_order'] = Carbon::now();
        $bill['id_user'] = Auth::guard('frontend')->user()->id;
        $bill['total'] = (int)Session::get('cart')->totalPrice;
        DB::beginTransaction();
        try{
            $id = $this->billRepository->create($bill);
            $this->billDetailRepository->addList($id);
            DB::commit();
            return redirect()->route('page.checkout')->with('success','Create Bill success ');
        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->route('page.checkout')->with('error','Create Bill error ');
        }
    }
}
