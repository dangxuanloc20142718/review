<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
   public function addCart(Request $request){
       if($request->ajax()){
           $id = $request->id;
           $sl = $request->quantity;
           $product = Product::find($id);
           $data [] = $product;
           $data [] = $sl;
           $datas [] = $data;
           if(Session::has('cart')){
               $datas [] = Session::get('cart');
           }
           Session::put('cart',$datas);
           return count(Session::get('cart'));
       }
   }
   public function index(){
        $listProduct = Product::all();
        return view('Abcd',compact('listProduct'));
   }
}
