<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductRepository;

class DetailController extends Controller
{
    protected $productRepository;
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index($id){
        $product = $this->productRepository->find($id);
        $listComment = $this->productRepository->getCommentProduct($id);
        $listProductRelative = $this->productRepository->getProductRelative($id);
        $listProductBestSale = $this->productRepository->bestSale();
        $listNewProduct = $this->productRepository->getNewProduct();
        return view('frontend.detail.index',compact('product','listComment','listProductRelative','listProductBestSale','listNewProduct'));
    }
}
