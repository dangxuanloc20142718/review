<?php

namespace App\Http\Controllers;

use App\Repositories\BannerRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PageController extends Controller
{
    public $repository;
    public $productRepositpory;
    public function __construct(BannerRepository $repository,ProductRepository $productRepository)
    {
        $this->repository = $repository;
        $this->productRepositpory = $productRepository;
    }

    public function index (){
        $listBanner = $this->repository->all();
        $newProduct = $this->productRepositpory->getNewProduct();
        $topProduct = $this->productRepositpory->getTopProduct();
        return view('frontend.homepage.index',compact('listBanner','newProduct','topProduct'));
    }
    public function getLogin(){
        if(Auth::guard('frontend')->check()){
            return redirect()->route('page.home');
        }else{
            return view('frontend.login.index');
        }
    }
    public function postLogin(Request $request){
            $info = $request->only(['email','password']);
            if(Auth::guard('frontend')->attempt($info)){
                return redirect()->route('page.home');
            }else{
                Session::flash('messages','login failure');
                return redirect()->route('getLogin');
            }
    }
    public function getLogout(){
        if(Auth::guard('frontend')->check()){
            Auth::guard('frontend')->logout();
            Session::flush();
            return redirect()->route('getLogin');
        }
    }
}
