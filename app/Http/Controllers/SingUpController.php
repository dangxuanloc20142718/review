<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Repositories\UsersRepository;

class SingUpController extends Controller
{
    protected $userRepository;
    public function __construct(UsersRepository $userRepository)
    {
            $this->userRepository = $userRepository;
    }

    public function getSingUp(){
        return view('frontend.singup.index');
    }
    public function postSingUp(LoginRequest $request){
        $info = $request->only('email','full_name','address','phone','password');
        $info['password'] = bcrypt($info['password']);
        $info['role'] = config('const')['role']['user'];
        try{
            $this->userRepository->create($info);
            return redirect()->route('page.getSingUp')->with('messages',config('const')['messages']['success']);
        }catch (\Exception $e){
            return redirect()->route('page.getSingUp')->with('messages',config('const')['messages']['errors']);
        }

    }
}
