<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{
    public function index(){
        return view('frontend.contact.index');
    }
    public function sendMail(Request $request){
        $info = $request->all();
        Mail::send('element.mail',['info'=>$info], function($message) use($info){
            $message->to($info['email'], 'User')->subject($info['subject']);
        });
        Session::flash('flash_message', 'Send message successfully!');
        $request->flash();
        return redirect()->route('page.contact');
    }
}
