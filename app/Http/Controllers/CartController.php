<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function add(Request $request)
    {
        if ($request->json()) {
            $id_product = $request->id;
            $oldCard = Session::get('cart');
            $cart = new Cart($oldCard);
            $product = $this->productRepository->find($id_product);
            $cart->add($product, $id_product);
            Session::put('cart', $cart);
            $totalCount = count($cart->items);
            return response()->json([
                'cart' => view('element.add_to_cart', compact('cart'))->render(),
                'totalCount' => $totalCount
            ]);
        }
    }

    public function delete(Request $request)
    {
        if ($request->json()) {
            $id = $request->id;
            $cart = Session::get('cart');
            if(array_key_exists($id,$cart->items)){
                $cart->removeItem($id);
            }
            Session::put('cart', $cart);
            return response()->json([
                'id' => $id,
                'qty' => count($cart->items),
                'price' => $cart->totalPrice
            ]);
        }

    }

    public function detail()
    {
        return view('frontend.shopping_cart.index');
    }

    public function changeQuantity(Request $request)
    {
        if ($request->json()) {
            $id = $request->id;
            $type = $request->type;
            $item = $this->productRepository->find($id);
            $totalPrice = '';
            if($type == '+'){
                $newCart = Session::get('cart');
                $newCart->add($item,$id);
                Session::put('cart',$newCart);
                $totalPrice = $newCart->items[$id]['price'];
                $orderTotal = $newCart->totalPrice;
            }else{
                $newCart = Session::get('cart');
                $newCart->reduceByOne($id);
                Session::put('cart',$newCart);
                $totalPrice = $newCart->items[$id]['price'];
                $orderTotal = $newCart->totalPrice;
            }
            return response()->json([
                'type' => $type,
                'total' => $totalPrice,
                'orderTotal' => $orderTotal
            ]);
        }
    }

    public function deleteItem($id){
        $cart = Session::get('cart');
        if(array_key_exists($id,$cart->items)){
            $cart->removeItem($id);
            Session::put('cart',$cart);
        }
        return redirect()->route('cart.detail');
    }
}
