<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use Illuminate\Pagination\Paginator;

class CategoryController extends Controller
{
    public $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index($id)
    {
        $id_type = $id;
        $newProduct = $this->repository->getNewProductOfCategory($id);
        $topProduct = $this->repository->getTopProductOfCategory($id);
        $categoryName = $this->repository->find($id)->name;
        return view('frontend.category.index', compact('newProduct', 'topProduct', 'categoryName','id_type'));
    }
    public function paginate(Request $request){
        if($request->name == 'top'){
            $page = $request->page;
            $id_type = $request->id_type;
            Paginator::currentPageResolver(function() use ($page) {
                return $page;
            });
            $topProduct = $this->repository->getTopProductOfCategory($id_type);
            return $topProduct;
        }
        if($request->name == 'new'){
            return 'new';
        }
    }
}
