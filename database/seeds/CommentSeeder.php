<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $ins_date = Carbon::now()->format('Y-m-d H:i:s');
        $datas =[];
        for($i = 0; $i <10 ; $i++){
            $data['id_product'] = rand(50,60);
            $data['id_user'] = rand(47,60);
            $data['comment'] = $faker->realText( 100, 2);
            $data['ins_id'] = 1;
            $data['ins_date'] = $ins_date;
            $datas[] = $data;
        }
        DB::table('comment')->insert($datas);
    }
}
