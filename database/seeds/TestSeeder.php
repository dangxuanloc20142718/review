<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // DB::table('customer')->truncate();

        // Create virtual DB
        $faker = Faker::create();
        $data = [
        	[
        	'name' => "dangxuanloc96",
        	'password' => Bcrypt("dangxuanloc1996"),
        	'rule' => 1
        	],
        	[
        	'name' => "dangvanc",
        	'password' => Bcrypt("dangxuanloc1996"),
        	'rule' => 2
        	],
        ];
        DB::table("customer")->insert($data);

    }
}
