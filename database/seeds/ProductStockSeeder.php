<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProductStockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_stock')->truncate();
        $faker = Faker::create();
        $startDate = Carbon::now()->startOfMonth();
        $ennDate = Carbon::now()->endOfMonth();
        $datas =[];
        for($j = 1;$j<=20;$j++){
            if($j == 10){
                continue;
            }else{
                for($i = 0; $i <$ennDate->day ; $i++){
                    $data['id_product'] = $j;
                    $data['stock_date'] = $startDate;
                    $data['stock_count'] = rand(100,200);
                    $data['sale_count'] = rand(40,100);
                    $data['stock_status'] = '1';
                    $data['ins_id'] = 1;
                    $data['ins_date'] = carbon::now();
                    $datas[] = $data;
                    $startDate->addDays();
                }
            }
        }
        DB::table('product_stock')->insert($datas);
    }
}
