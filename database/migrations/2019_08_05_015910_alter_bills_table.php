<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bills',function(Blueprint $table){
            $table->integer('id_user')->unsigned()->after('id');
            $table->string('name')->after('note');
            $table->enum('gender',['0','1'])->after('name');
            $table->string('email')->after('gender');
            $table->string('address')->after('email');
            $table->string('phone_number')->after('address');

            //foreign key
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
