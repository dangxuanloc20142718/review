<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_product');
            $table->dateTime('stock_date');
            $table->integer('stock_count');
            $table->integer('sale_count');
            $table->enum('stock_status',['0','1']);
            $table->integer('ins_id');
            $table->dateTime('ins_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('ups_id')->nullable();
            $table->dateTime('ups_date')->nullable();
            //foreignkey
            $table->foreign('id_product')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_stock');
//        Schema::table('product_stock', function (Blueprint $table) {
//        });
    }
}
